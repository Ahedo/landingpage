const availableJackpots = {jackpotName: "Big Series",
    jackpotValue: "1231565.48 GBP",
    currency: "GBP",
    lastUpdatedDate: "02/03/19 11:31:29 ET",
    subJackpotDetails: 
      {
        subjackpotName: "The Big One Super Cash",
        subjackpotValue: "2407.05 GBP",
        subjackpotCurrency: "GBP"
      }
    };