class CollapsableElementController {
    constructor(element) {}

    static  isItCollapsable(e){
        const element = e.target;
        if (element.classList.contains("term-and-conditions-box__terms-and-conditions__collapsable")){
            e.preventDefault();
            this.collapseElement(element);
        }
    }

    static collapseElement(element) {
        if (element.classList.contains("term-and-conditions-box__terms-and-conditions--collapsed")) {
            element.classList.remove("term-and-conditions-box__terms-and-conditions--collapsed");
            element.classList.add("term-and-conditions-box__terms-and-conditions--extended");
        } else  {
            element.classList.remove("term-and-conditions-box__terms-and-conditions--extended");
            element.classList.add("term-and-conditions-box__terms-and-conditions--collapsed");
        }
    }
}