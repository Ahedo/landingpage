class JackpotController {
    
    constructor(className, value = 0, clockFace = "Counter") {
        this._className = className;
        this._value = value;
        this._clockFace = clockFace;
      }

      startJackpot(){
        const jackpotClass = "."+ this._className; 
        
        let jackpot = $(jackpotClass).FlipClock(this._value, {
            clockFace: this._clockFace
        });
    
        setTimeout(function() {
            setInterval(function() {
                jackpot.increment();
            }, 1000);
        });
      }

}