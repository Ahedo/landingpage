
function CollapsableElementController (){}

    CollapsableElementController.isItCollapsable = function(e) {
        var element = e.target;
        if (element.classList.contains("term-and-conditions-box__terms-and-conditions__collapsable")){
            e.preventDefault();
            CollapsableElementController.collapseElement(element);
        }
    }

    CollapsableElementController.collapseElement = function(element){
        if (element.classList.contains("term-and-conditions-box__terms-and-conditions--collapsed")) {
            element.classList.remove("term-and-conditions-box__terms-and-conditions--collapsed");
            element.classList.add("term-and-conditions-box__terms-and-conditions--extended");
        } else  {
            element.classList.remove("term-and-conditions-box__terms-and-conditions--extended");
            element.classList.add("term-and-conditions-box__terms-and-conditions--collapsed");
        }
    }



